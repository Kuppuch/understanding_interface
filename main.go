package main

import (
	"fmt"
	"image"
	"image/png"

	"io"
	"log"
	"os"
)

type Sypher struct {
	io.Writer
	key string
}

type DeSypher struct {
	io.Reader
	key string
}

type Counter struct {
	io.Writer
	cnt int
}

func (c *Counter) Write(p []byte) (n int, err error) {
	c.cnt++
	return c.Writer.Write(p)
}

func (s *Sypher) Write(p []byte) (n int, err error) {
	for i := range p {
		p[i]++
	}
	return s.Writer.Write(p)
	//return len(p),nil
}

func (d *DeSypher) Read(p []byte) (int, error) {
	n, err := d.Reader.Read(p)
	if err == nil && n > 0 {
		for i := range p[:n] {
			p[i]--
		}
	}
	return n, err
}

func main() {
	img := image.NewNRGBA(image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: 512, Y: 512}})
	var f, err = os.Create("1.png")

	if err != nil {
		panic(err)
	}

	var c = &Counter{Writer: f}
	//if err := png.Encode(c, img); err != nil {
	//	log.Fatalf("%v", err)
	//}

	var s = &Sypher{Writer: c, key: "key"}
	if err := png.Encode(s, img); err != nil {
		log.Fatalf("%v", err)
	}
	if err := f.Close(); err != nil {
		log.Fatalf("%v", err)
	}
	fmt.Println("число", c.cnt)

	/// раскодируем

	file, err := os.Open("1.png")
	if err != nil {
		log.Fatalf("%v", err)
	}
	var d = &DeSypher{Reader: file, key: "key"}
	dimg, format, err := image.DecodeConfig(d)
	if err != nil {
		log.Fatalf("%v", err)
	}
	fmt.Printf("картинка: %v %v \n формат %v", dimg.Width, dimg.Height, format)

}
